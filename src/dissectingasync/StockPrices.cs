﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace _00_dissectingasync
{
    public class StockPrices
    {
        public Dictionary<string, decimal> _stockPrices;
        public Task<decimal> GetStockPriceForAsync(string companyId)
        {
            //await InitiallizeMapIfNeededAsync();
            //_stockPrices.TryGetValue(companyId, out var result);
            //return result;
            var stateMachine = new GetStockPriceForAsync_StateMachine(this, companyId);
            stateMachine.Start();
            return stateMachine.Task;
        }

        public async Task InitiallizeMapIfNeededAsync()
        {
            if (_stockPrices != null)
            {
                return;
            }

            await Task.Delay(42);

            _stockPrices = new Dictionary<string, decimal>() { ["MSFT"] = 42 };
        }
    }
}
