﻿
namespace System.Runtime.CompilerServices
{
    public class AsyncVoidMethodBuilder
    {
        public AsyncVoidMethodBuilder()
        {
            Console.WriteLine(".ctor");
        }

#pragma warning disable CS0436 // 类型与导入类型冲突
        public static AsyncVoidMethodBuilder Create()
#pragma warning restore CS0436 // 类型与导入类型冲突
        {
#pragma warning disable CS0436 // 类型与导入类型冲突
            return new AsyncVoidMethodBuilder();
#pragma warning restore CS0436 // 类型与导入类型冲突
        }

        public void SetResult()
        {
            Console.WriteLine("SetResult");
        }

        public void Start<TStateMachine>(ref TStateMachine stateMachine)
            where TStateMachine : IAsyncStateMachine
        {
            Console.WriteLine("Start");
            stateMachine.MoveNext();
        }

        public void AwaitOnCompleted<TAwaiter, TStateMachine>(ref TAwaiter awaiter, ref TStateMachine stateMachine)
            where TAwaiter : INotifyCompletion
            where TStateMachine : IAsyncStateMachine
        {

        }

        public void AwaitUnsafeOnCompleted<TAwater, TStateMachine>(ref TAwater awater, ref TStateMachine stateMachine)
            where TAwater : INotifyCompletion
            where TStateMachine : IAsyncStateMachine
        {

        }

        public void SetException(Exception e)
        {

        }

        public void SetStateMachine(IAsyncStateMachine stateMachine)
        {

        }
    }
}
