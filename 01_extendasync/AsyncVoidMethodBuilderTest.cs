﻿//using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace _01_asyncvoidsample
{
    public class AsyncVoidMethodBuilderTest
    {
        [Fact]
        public void RunAsyncVoid()
        {
            Console.WriteLine("Before VoidAsync");
            VoidAsync();
            Console.WriteLine("After VoidAsync");
            async void VoidAsync() { }
        }
    }
}
